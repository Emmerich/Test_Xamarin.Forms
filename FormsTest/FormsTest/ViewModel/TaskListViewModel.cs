﻿using FormsTest.Message;
using FormsTest.Model;
using FormsTest.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Linq;
using Xamarin.Forms;

namespace FormsTest.ViewModel
{
    public class TaskListViewModel : ViewModelBase
    {
        public TaskListViewModel(INavigation navigation)
            :base(navigation)
        {
            InitializeTestData();
            SubscribeToMessage();
        }

        private void InitializeTestData()
        {
            List<TaskModel> taskList = new List<TaskModel>();
            Random random = new Random();
            var description = @"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   
Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.";

            for (int i = 0; i<30; i++)
            {
                var newInvoice = new InvoiceModel(String.Format("Kunde {0}", i + 1), Math.Round(random.NextDouble() * 1000, 2));
                
                var newTask = new TaskModel(
                    String.Format("Auftrag {0}", i + 1),
                    String.Format("Musterstr. {0}{1}12345 Musterort_{0}", i + 1, Environment.NewLine),
                    description, 
                    newInvoice);
                taskList.Add(newTask);
            }
            TaskList = new ObservableCollection<TaskModel>(taskList);
        }
    
        public ObservableCollection<TaskModel> TaskList { get; set; }

        private String _PageTitle = "Auftragliste";
        public String PageTitle
        {
            get
            {
                return _PageTitle;
            }
            set
            {
                if (value == _PageTitle)
                    return;

                _PageTitle = value;
                NotifyPropertyChanged();
            }
        }

        private TaskModel _SelectedTask;
        public TaskModel SelectedTask
        {
            get
            {
                return _SelectedTask;
            }
            set
            {
                if (value == _SelectedTask)
                    return;

                _SelectedTask = value;

                Navigation.PushAsync(new TaskDetailView(value));

                NotifyPropertyChanged();
            }
        }

        private void SubscribeToMessage()
        {
            MessagingCenter.Subscribe<TaskFinishedMessage>(this, "", (TaskFinishedMessage) =>
            {
                TaskList.Remove(TaskList.ToList().Find(x => x.TaskId == TaskFinishedMessage.FinishedTask.TaskId));
            });
        }
    }
}