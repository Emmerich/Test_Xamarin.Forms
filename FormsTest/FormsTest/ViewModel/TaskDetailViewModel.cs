﻿using FormsTest.Message;
using FormsTest.Model;
using FormsTest.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace FormsTest.ViewModel
{
    public class TaskDetailViewModel : ViewModelBase
    {
        public TaskDetailViewModel(INavigation navigation, TaskModel taskModel)
            : base(navigation)
        {
            Task = taskModel;

            CreateInvoiceCommand = new Command(() =>
            {
                Navigation.PushAsync(new InvoiceView(Task.Invoice));
            });

            FinishTaskCommand = new Command(() =>
            {
                MessagingCenter.Send(new TaskFinishedMessage(Task), "");
                Navigation.PopAsync();
            });

            SubscribeToMessage();
        }

        public Command CreateInvoiceCommand { get; }
        public Command FinishTaskCommand { get; }

        private TaskModel _Task;
        public TaskModel Task
        {
            get
            {
                return _Task;
            }
            set
            {
                if (value == _Task)
                    return;

                _Task = value;

                CheckInvoiceStatus();

                NotifyPropertyChanged();
            }
        }

        private string _InvoiceStatusText = "Die Rechnung ist noch nicht abgeschlossen";
        public string InvoiceStatusText
        {
            get
            {
                return _InvoiceStatusText;
            }
            set
            {
                if (value == _InvoiceStatusText)
                    return;

                _InvoiceStatusText = value;
                NotifyPropertyChanged();
            }
        }

        private bool _InvoiceState;
        public bool InvoiceState
        {
            get
            {
                return _InvoiceState;
            }
            set
            {
                if (value == _InvoiceState)
                    return;

                _InvoiceState = value;
                ShowCreateInvoice = value;
                NotifyPropertyChanged();
            }
        }
        public bool ShowCreateInvoice
        {
            get
            {
                return !_InvoiceState;
            }
            set
            {
                NotifyPropertyChanged();
            }
        }

        private void SubscribeToMessage()
        {
            MessagingCenter.Subscribe<InvoiceFinishedMessage>(this, "", (InvoiceFinishedMessage) =>
            {
                if(Task.Invoice.InvoiceId == InvoiceFinishedMessage.FinishedInvoice.InvoiceId)
                    Task.Invoice = InvoiceFinishedMessage.FinishedInvoice;

                CheckInvoiceStatus();
            });
        }

        private void CheckInvoiceStatus()
        {
            CultureInfo ci = new CultureInfo("de-DE");
            if (Task.Invoice.ExecutionDate != DateTime.MinValue)
            {
                InvoiceStatusText = String.Format("Die Rechnung wurde am {0} abgeschlossen", Task.Invoice.ExecutionDate.ToLocalTime().ToString("d", ci));
                InvoiceState = true;
                NotifyPropertyChanged("Task");
            }
            else
            {
                InvoiceStatusText = "Die Rechnung ist noch nicht abgeschlossen";
                InvoiceState = false;
            }
        }
    }
}