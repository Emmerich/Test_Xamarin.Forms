﻿using System;
using System.Collections.Generic;
using System.Text;
using FormsTest.Model;
using Xamarin.Forms;
using FormsTest.Message;

namespace FormsTest.ViewModel
{
    public class InvoiceViewModel : ViewModelBase
    {
        public InvoiceViewModel(INavigation navigation, InvoiceModel invoice) 
            : base(navigation)
        {
           Invoice = invoice;

            FinishInvoiceCommand = new Command(() =>
            {
                Invoice.ExecutionDate = DateTime.Now;
                MessagingCenter.Send(new InvoiceFinishedMessage(Invoice), "");
                Navigation.PopAsync();
            });
        }

        public Command FinishInvoiceCommand { get; }

        private InvoiceModel _Invoice;
        public InvoiceModel Invoice
        {
            get
            {
                return _Invoice;
            }
            set
            {
                if (value == _Invoice)
                    return;

                _Invoice = value;
                NotifyPropertyChanged();
            }
        }

        private string _PageTitle = "Rechnung";
        public string PageTitle
        {
            get
            {
                return _PageTitle;
            }
            set
            {
                if (value == _PageTitle)
                    return;

                _PageTitle = value;
                NotifyPropertyChanged();
            }
        }


    }
}
