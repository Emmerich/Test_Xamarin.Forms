﻿using FormsTest.Model;
using FormsTest.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FormsTest.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TaskDetailView : ContentPage
	{
        private object taskModel;

        public TaskDetailView (TaskModel model)
		{
			InitializeComponent ();
            BindingContext = new TaskDetailViewModel(Navigation, model);
		}
    }
}