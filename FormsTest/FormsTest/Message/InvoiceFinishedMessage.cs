﻿using FormsTest.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace FormsTest.Message
{
    public class InvoiceFinishedMessage
    {
        public InvoiceFinishedMessage(InvoiceModel invoice)
        {
            FinishedInvoice = invoice;
        }

        public InvoiceModel FinishedInvoice { get; set; }
    }
}
