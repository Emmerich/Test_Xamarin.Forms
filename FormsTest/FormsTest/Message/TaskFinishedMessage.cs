﻿using FormsTest.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace FormsTest.Message
{
    public class TaskFinishedMessage
    {
        public TaskFinishedMessage(TaskModel task)
        {
            FinishedTask= task;
        }

        public TaskModel FinishedTask { get; set; }
    }
}
