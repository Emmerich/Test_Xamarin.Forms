﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace FormsTest.Model
{
    public class TaskModel
    {
        public TaskModel(string name, string location, string description, InvoiceModel invoice)
        {
            TaskId = Guid.NewGuid();
            TaskName = name;
            Location = location;
            Description = description;
            Invoice = invoice;
        }

        public Guid TaskId { get; set; }
        public String TaskName { get; set; }
        public String Location { get; set; }
        public String Description { get; set; }
        public InvoiceModel Invoice { get; set; }
    }
}
