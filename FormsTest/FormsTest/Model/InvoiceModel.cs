﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FormsTest.Model
{
    public class InvoiceModel
    {
        public InvoiceModel(string customerName, double amount)
        {
            InvoiceId = Guid.NewGuid();
            CustomerName = customerName;
            Amount = amount;
        }
        public Guid InvoiceId { get; set; }
        public string CustomerName { get; set; }
        public double Amount { get; set; }
        public DateTime ExecutionDate { get; set; }
        public String Extra { get; set; }
    }
}
