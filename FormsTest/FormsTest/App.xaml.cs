﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace FormsTest
{
	public partial class App : Application
	{
		public App ()
		{
            //Current.Resources = new ResourceDictionary
            //{
            //    { "DateTimeFormatConverter", new DateTimeFormatValueConverter()}
            //};

            InitializeComponent();
            
            MainPage = new NavigationPage(new FormsTest.View.TaskListView());

            
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
